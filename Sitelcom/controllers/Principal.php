<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Principal extends CI_Controller {
        public function __construct() {
            parent::__construct();
            $this->load->model('modelo');
            $this->load->library('cezpdf');
            $this->load->helper('pdf_helper');
        }
	public function index()
	{   
        //Debo revisar si la persona está logeada o no...
        //Si está logeada paso a la agenda, sino al login!
        if($this->session->userdata('logged_in')){
            $data['nombre'] = $this->session->userdata('nombre');
            $data['perfil'] = $this->session->userdata("perfil");
            $this->load->view("header",$data);
            $this->load->view('Agenda',$data);
            $this->load->view("footer");
        }
        else{
            $data['error'] = "";
            $this->load->view('inicio',$data);
        }
        //$this->load->view('Agenda');
	}
    function validaUsuario(){
        $usuario = $this->input->post("rut");
        $clave = md5($this->input->post('clave'));
        $valor = $this->modelo->consultaUsuario($usuario,$clave)->num_rows();

        if($valor == 1){
            //Buscar nombre del usuario que se ha conectado....
            $res = $this->modelo->consultaUsuario($usuario,$clave)->result();
            foreach ($res as $row) {
                $nombre = $row->nombre;
                $perfil = $row->perfil;
            }
            $data   =   array(
                'usuario'  => $usuario,
                'nombre'    => $nombre,
                'logged_in' => TRUE,
                'perfil' => $perfil
            );
        $this->session->set_userdata($data);
            $data['nombre'] = $this->session->userdata('nombre');
            $data['perfil'] = $this->session->userdata("perfil");
            $this->load->view("header",$data);
            $this->load->view('Agenda',$data);
            $this->load->view("footer");
        }else{
            $data['error'] = "Usuario no registrado";
            $this->load->view('inicio',$data);
        }

    }
    function Respaldar()
    {
        $this->load->dbutil();
        $prefs = array(
                'tables'      => array('administracion', 'ant_morbidos', 'ant_oftalmologico_f', 'ant_oftalmologico_p', 'biomicroscopia', 'consultas', 'diagnostico', 'examen_oftalmologico', 'ficha', 'motivoconsulta', 'recetal', 'recetam', 'usuarios'),  // Arreglo de tablas para respaldar.                 'ignore'      => array(),           // Lista de tablas para omitir en la copia de seguridad
                'format'      => 'zip',             // gzip, zip, txt
                'filename'    => 'AgendaNueva_'.date("d-m-Y"),    // Nombre de archivo - NECESARIO SOLO CON ARCHIVOS ZIP
                'add_drop'    => TRUE,              // Agregar o no la sentencia DROP TABLE al archivo de respaldo
                'add_insert'  => TRUE,              // Agregar o no datos de INSERT al archivo de respaldo
                'newline'     => "\n"               // Caracter de nueva línea usado en el archivo de respaldo
              );
        $copia_de_seguridad =& $this->dbutil->backup($prefs);
        $this->load->helper('file');
        write_file('D:\\Respaldo.zip', $copia_de_seguridad);
        //return null;
    }
    function ingresarPersonal(){
        $res = $this->modelo->ingresarPersonal($nombre,$rut,$apellidos,$fecha_nacimiento,$direccion,$telefono,$email,$patente,$fonasa,$afp,$categoria,$hijos,$tipo_contrato,$fecha_inicio,$fecha_termino,$estado,$causal);
        if($res == 0){
            echo json_encode(array("info"=>"ingreso"));
        }else
        {
            echo json_encode(array("info"=>"no ingreso"));
        }
    }
    function logout()
    {
        //$this->Respaldar();
        $this->session->sess_destroy();
        redirect(base_url());
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */